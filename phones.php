{
"records": [
  {
    "ID" : "1",
    "Producer" : "Iphone",
    "Model" : "5",
    "Price" : 70000,
    "Memory" : "16",
    "Color" : "Space",
    "Connectivity" : "4g",
    "Image" : "img/items/android1.jpg",
    "incard":false
  },
  {
    "ID" : "2",
    "Producer" : "Samsung",
    "Model" : "s5",
    "Price" : 80000,
    "Memory" : "16",
    "Color" : "Space",
    "Connectivity" : "4g",
  "Image" : "img/items/iphone4.jpg",
  "incard":false
  },
  {
    "ID" : "3",
    "Producer" : "Iphone",
    "Model" : "4s",
    "Price" : 50000,
    "Memory": "16",
    "Color": "Space",
    "Connectivity": "4g",
    "Image" : "img/items/iphone3.jpg",
    "incard": false
  },
  {
    "ID" : "4",
    "Producer" : "Nokia",
    "Model" : "Lumia",
    "Price" : 80000,
    "Memory": "16",
    "Color": "White",
    "Connectivity": "3g",
    "Image" : "img/items/android1.jpg",
    "incard": false
  }

  ]
}