<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery-1.11.3.js"></script>
	<script src="js/jquery-ui/jquery-ui.min.js"></script>
	<script src="angular-1.4.7/angular.min.js"></script>
   <script src="css/less.min.js" type="text/javascript"></script>

	<script src="js/jquery.js"></script>
	<link rel="stylesheet/less" type="text/css" href="css/mstyle.less" />
	<script src="css/less.min.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="css/allitems.css">
	<meta charset="UTF-8">
	<title>Project</title>
	</head>
<body ng-app="myApp" ng-controller="Ctrl">



	<div id="navd">
	<a href="intro.php"><img id="logo" src="img/logo.png"></img></a>
		<ul class="navanchik">
			<a href="main_page.php"><li>ГЛАВНАЯ СТРАНИЦА</li></a>
			<a href="#"><li>УСЛУГИ</li></a>
			<a href="#"><li>АКСЕССУАРЫ</li></a>
			<a href="#"><li id="account-hover">АККАУНТ</li></a>
			<a href="#" id="last"><li>ПОИСК</li></a>
		</ul>
	</div><!--nav DIV-->
	<div id="account">
		<div id="inacc">
			<img src="img/profile.png">
			<input id="sign" type="submit" value="ВОЙТИ">
			<p>Нет аккаунта?<a href="#">Регистрация<a></p>
		</div>
		<ul>
			<a href="#"><li>Корзина</li></a>
			<a href="#"><li>Аумау</li></a>
			<a href="#"><li>Аумау</li></a>
		</ul>
	</div>
	<div class="search">
		<p>ENTER SEARCH BELOW</p><br>
		<input placeholder="SEARCH"type="text" ng-model="test" name="search">
	</div>
	<div class="container1" >

		<div class="blocks" id="filterblock">
			<div class="filter">
			<ul>
			<li><label><input type="checkbox" ng-model="check.Iphone">Iphone
		    <p>(4)</p>
		  </label></li>
		  	<li><label> <input type="checkbox" ng-model="check.Samsung"> Samsung

		   <p>(10)</p></label></li>
		    <li><label><input type="checkbox" ng-model="check.Nokia">Nokia

		   <p>(3)</p></label></li>
		   </ul>
		   <hr>
		   <label class="min">MIN PRICE</label><br/>
		    <input type="range" min="0" max="300000" ng-model="minPrice"/><div class="bg">{{minPrice}}</div>
		 	<br/>
		     <label class="min">MAX PRICE</label><br/>
		     <input type="range" min="0" max="300000" ng-model="maxPrice"/><div class="bg">{{maxPrice}}</div>
		   </label>
		</div>
		Drag and drop your order here
		<img id="cart" src="img/cart.png">
		<div class="korzina" id="korzina1"  ondrop="drop(event)" ondragover="allowDrop(event)" >

         </div>
         <div id="res">0</div>
         <button id="cartbtn1">КОРЗИНА</button>
     </div>
		<div class="item-container" id="items-cont">

		<a ng-show="check.Iphone" ng-repeat="x in phones | filter: test | filter : 'Iphone'| filter:priceRangeFilter ">
			<div class="item" ng-attr-id="{x.ID}" draggable="true" ondragstart="drag(event)">
			<img src="img/items/1.jpg" draggable="false" ></img>
			<p >{{x.Producer + x.Model}}</p>
			<p >{{x.Price}}</p>
			</div></a>
		<a ng-show="check.Samsung" ng-repeat="x in phones | filter: test | filter : 'Samsung' | filter:priceRangeFilter">
			<div class="item" ng-attr-id="{x.ID}" draggable="true" ondragstart="drag(event)">
			<img src="img/items/1.jpg" draggable="false" >
			<p>{{x.Producer+ x.Model}}</p>
			<p>{{x.Price}}</p>
			</div></a>
		<a ng-show="check.Nokia" ng-repeat="x in phones | filter: test | filter : 'Nokia' | filter:priceRangeFilter">
			<div class="item" ng-attr-id="{x.ID}"draggable="true" ondragstart="drag(event)">
			<img src="img/items/1.jpg" draggable="false" >
			<p>{{x.Producer+ x.Model}}</p>
			<p>{{x.Price}}</p>
			</div></a>
		</div>

	</div>
	<footer id="footer">
		<ul id="pp">
			<lh>Policy Info</lh>
			<a href="#"><li>Privacy Plocy</li></a>
			<a href="#"><li>Terms of Sale</li></a>
			<a href="#"><li>Terms of Use</li></a>
			<a href="#"><li>Report Abuse</li></a>
			<a href="#"><li>CSR Policy</li></a>
			<a href="#"><li>Media Communication</li></a>
			<a href="#"><li>FAQ</li></a>
		</ul>
		<ul id="co">
			<lh>Company</lh>
			<a href="#"><li>About Us</li></a>
			<a href="#"><li>Core Values</li></a>
			<a href="#"><li>Press</li></a>
			<a href="#"><li>Careers</li></a>
			<a href="#"><li>Blog</li></a>
			<a href="#"><li>Sitemap</li></a>
			<a href="#"><li>Contact Us</li></a>
		</ul>
		<ul id="pa">
			<lh>Partners</lh>
			<a href="#"><li>Freecharge</li></a>
			<a href="#"><li>Ebay</li></a>
			<a href="#"><li>Amazon</li></a>
			<a href="#"><li>PayPal</li></a>
		</ul>
		<ul id="tb">
			<lh>TechStore Business</lh>
			<a href="#"><li>Gift Voucher</li></a>
			<a href="#"><li>Sell on TechStore</li></a>
			<a href="#"><li>Media Enquiries</li></a>
			<a href="#"><li>Be An Affiliate</li></a>
			<a href="#"><li>Deal of the Day</li></a>
			<a href="#"><li>TechStore Launchpad</li></a>
		</ul>
		<img src="img/logo.png">
		<ul id="sw">
			<li><img id="tw" src="img/swicons/grey/1g.svg"></li>
			<li><img id="fb" src="img/swicons/grey/2g.svg"></li>
			<li><img id="in"src="img/swicons/grey/3g.svg"></li>
			<li><img id="yo" src="img/swicons/grey/4g.svg"></li>
			<li><img id="p" src="img/swicons/grey/5g.svg"></li>
			<li><img id="go" src="img/swicons/grey/6g.svg"></li>
		</ul>
	<div id="infoot"><p>© 2015 <a href="#">TECH STORE</a></p></div>
	</footer>

	<div class="login-container">
		<div id="signin">
			 <h1>Войти</h1>
			 <input type="text" placeholder="ВВЕДИТЕ ЛОГИН">
			 <input	type="password" placeholder="ВВЕДИТЕ ПАРОЛЬ">
			 <input type="submit" value="ВОЙТИ"><a id="forgotbtn" href="#">Забыли пароль?</a>
			 <div id="divider"><hr class="line"><p >OR</p><hr class="line"></div>
			 <div class="face">
			 	<p>Facebook</p>
			 </div>
			 <div class="goo">
			 	<p>Goolge +</p>
			 </div>
			 <p>Нет аккаунта?<a id="regbtn">Регистрация</a></p>
		</div>
		<div id="forgot">
			<h1>Забыли пароль</h1>
			<input type="text" placeholder="ВВЕДИТЕ EMAIL">
			<input type="submit" id="frgtsubmit" value="ВВЕСТИ">
		</div>
		<div id="register">
			<h1>Регистрация</h1>
			<input type="text" placeholder="ИМЯ">
			<input type="text" placeholder="ФАМИЛИЯ">
			<input type="text" placeholder="ЛОГИН">
			<input type="text" placeholder="EMAIL">
			<input type="password" placeholder="ПАРОЛЬ">
			<input type="password" placeholder="ПОДТВЕРДИТЕ ПАРОЛЬ">
			<input type="submit" id="regsubmit" value="ВВЕСТИ">
		</div>

	</div>
	<div id="message" class="message">
		<p>На указанный вами email были высланы дальнейшие указания.</p>
	</div>
	<div id="message2" class="message">
		<p>Регистрация прошла успешно.</p>
	</div>
	<div id="back"></div>
	<div id="back"></div>
		<div id="cart1">

		<p id="empty">ВАША КОРЗИНА ПУСТА</p>

		</div>




	<script>
	var app = angular.module('myApp', []);
	app.controller('Ctrl', function($scope,$http) {

	$http.get("http://localhost/era/phones.php")
  	.success(function (response) {$scope.phones = response.records;});

  	$scope.check = {
       Iphone : true,
       Samsung : true,
       Nokia: true
     };


    $scope.minPrice = 0;
    $scope.maxPrice = 150000;
    $scope.priceRangeFilter = function (x) {
    	if(x.Price >= $scope.minPrice)
			{
			if(x.Price <= $scope.maxPrice){
				return true;
			}}
			else{
		return false;}

    };
    $scope.login = "";
    $scope.passw = "";
    $scope.sub=function(){

		localStorage.setItem("login", $scope.firstName );

		localStorage.setItem("pass", $scope.passw );

		document.getElementById("result").innerHTML = localStorage.getItem("login");

    }





});

</script>
<script>
var card=0;
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
	console.log(ev);
	var img=ev.toElement.children[0].getAttribute("src");
	var man=ev.toElement.children[1].innerHTML;
	var man2=ev.toElement.children[2].innerHTML;
    ev.dataTransfer.setData("text", man);
    ev.dataTransfer.setData("text2", man2);
    ev.dataTransfer.setData("text3", img);
}

function drop(ev) {
	if(card==0){
		document.getElementById("empty").remove();
	}
	card+=1;
    ev.preventDefault();
    var item = document.createElement("div");
	item.setAttribute('class','cartitem');
	var para = document.createElement("p");
	para.setAttribute('class','price');
	para.innerHTML=ev.dataTransfer.getData("text");

	var para1 = document.createElement("p");
	para1.setAttribute('class','price');
	para1.innerHTML=ev.dataTransfer.getData("text2");
	var del = document.createElement("img");
	del.setAttribute('class','delbtn');
	del.setAttribute('src','img/del.png');
	del.setAttribute('onclick','Delete(event)');
	var image = document.createElement("img");
	image.setAttribute('class','phonepic');
	var sors=ev.dataTransfer.getData("text3");

	image.setAttribute('src',''+sors);

	item.appendChild(para1);
	item.appendChild(para);
	item.appendChild(del);
	item.appendChild(image);
    document.getElementById("res").innerHTML=card;
    document.getElementById("cart1").appendChild(item);


    }

   function Delete(event) {
   		card-=1;
   		document.getElementById("res").innerHTML=card;
   		event.target.parentElement.remove();
   		if(card==0){
   			var empty=document.createElement("p");
   			empty.setAttribute('id','empty');
			empty.innerHTML="ВАША КОЗРИНА ПУСТА";
   			document.getElementById("cart1").appendChild(empty);
   		}
   }
function showk(ev) {


   }
</script>


</body>
</html>
